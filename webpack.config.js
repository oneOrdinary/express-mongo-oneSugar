const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
module.exports = {
  target: 'node',
  module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader',

				options: {
					presets: ['env']
				}
			},
		]
	},

	plugins: [
	  new UglifyJSPlugin(),
  ],

	entry: './app.js',

	output: {
		filename: 'bundle.js',
		path: __dirname+'./dist'
	},
  node: {
    fs: "empty",
    tls: "empty",
    net: "empty",
    module: "empty",
  }
};
