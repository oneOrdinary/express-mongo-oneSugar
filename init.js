/**
 * Created by hugo on 2018/8/17.
 */
const baseBeans = require('./beans')
const fs = require("fs")


isCatalogExistAndMake('application')
isCatalogExistAndMake('application/schema')
isCatalogExistAndMake('application/model')
isCatalogExistAndMake('application/controller')


baseBeans.map((v, i) => {

  let schemaPath = __dirname + '/application/schema/' + v.bean + '.js'
  let modelPath = __dirname + '/application/model/' + v.bean + '.js'
  let controllerPath = __dirname + '/application/controller/' + v.bean + '.js'
  //在app的schema下创建相应的文件
  fs.exists(modelPath, exists => {
    if (!exists) {
      console.log("model文件：" + v.bean + '.js 不存在，正在创建')
      let writable = fs.createWriteStream(modelPath)
      let modelStr = "" +
        "let mongoose = require('mongoose')\n" +
        "let " + v.bean + "Schema = require('../schema/" + v.bean + "')\n" +
        "let " + v.bean + " = mongoose.model('" + v.bean + "', " + v.bean + "Schema)\n" +
        "module.exports = " + v.bean
      writable.write(modelStr)

      writable.write("//实例才可调用的方法\n")
      writable.write(v.bean + "Schema.methods = {}\n")
      writable.write("//Schema即可调用的方法\n")
      writable.write(v.bean + "Schema.statics = {}\n")


      writable.end()
    }


  })

  fs.exists(schemaPath, exists => {
    if (!exists) {
      console.log("schema文件：" + v.bean + '.js 不存在，正在创建')
      let writable = fs.createWriteStream(schemaPath)

      writable.write("let mongoose = require('mongoose') \n")
      writable.write("let Schema = mongoose.Schema \n")

      writable.write("let ObjectId = Schema.Types.ObjectId \n")
      writable.write("let Number = Schema.Types.Number \n")

      writable.write("let " + v.bean + "Schema = new mongoose.Schema({ \n")

      let strParams = ""
      let refParams = ""
      let uniParams = ""
      let intParams = ""

      v.intField && v.intField.map((int, i) => {
        intParams += "\t" + int + ": Number(6),\n"
      })

      v.strField && v.strField.map((str, i) => {
        strParams += "\t" + str + ": String,\n"
      })

      v.refField && v.refField.map((ref, i) => {
        let refArr = ref.split('-')
        if (refParams.length === 1)
          refParams += "\t" + ref + ": {type:ObjectId,ref:\'" + ref + "\'},\n"
        else
          refParams += "\t" + refArr[0] + ": {type:ObjectId,ref:\'" + refArr[1] + "\'},\n"

      })

      v.uniField && v.uniField.map((ref, i) => {
        uniParams += "\t" + ref + ": {unique:true,type:String},\n"
      })

      writable.write(uniParams)
      writable.write(intParams)
      writable.write(strParams)
      writable.write(refParams)
      writable.write("})")

      writable.write("module.exports =" + v.bean + "Schema")


      writable.end()

    }


  })
  fs.exists(controllerPath, exists => {
    if (!exists) {
      console.log("controller文件：" + v.bean + '.js 不存在，正在创建')
      let writable = fs.createWriteStream(controllerPath)

      writable.write("const mongoose = require('mongoose')\n")
      writable.write("const " + v.bean + "Schema = require(\'../models/" + v.bean + "\') \n")
      writable.write("const Logger= require(\'../public/Logger\') \n")
      writable.write("const sysLogger= Logger.getLogger(\'sys\') \n")
      writable.write("const moment= require(\'moment\') \n")


      writable.end()

    }


  })
})


function isCatalogExistAndMake(path) {
  let realPath = __dirname + '/' + path
  fs.exists(realPath, exists => {
    if (!exists) {
      console.log('目录' + realPath + '不存在,正在创建……')
      fs.mkdir(realPath, err => {
        if (err) {
          console.log('创建目录' + realPath + '失败')
          return
        }
        console.log('创建目录' + realPath + '成功')
      })
    }

  })
}


