/**
 * Created by hugo on 2018/2/27.
 */

var User = require('../app/controller/user')
var Scene = require('../app/controller/scene')
var Rule = require('../app/controller/rule')
var Search = require('../app/controller/search')
var Log = require('../app/controller/log')
var RoleAndAuth = require('../app/controller/roleAndAuth')
var Condition = require('../app/controller/condition')
const log4js = require('log4js')
const logger = log4js.getLogger('router');
logger.level = 'debug'

const Logger = require('../public/untils/Logger')

module.exports = function (app) {
  app.post('/user/signup', User.signup)
  app.post('/user/signin', User.signin)
  app.post('/user/find', User.find)
  app.post('/user/access', User.access)
  app.post('/user/changeRole', User.changeRole)
  app.post('/user/remove', User.remove)
  app.post('/user/changePwd', User.changePwd)
  app.post('/user/logout', User.logout)
  app.get('/test1', User.test)

  app.post('/scene/create', Scene.create)
  app.post('/scene/delete', Scene.delete)
  app.get('/scene/find', Scene.find)
  app.post('/scene/update', Scene.update)


  app.post('/rule/create', Rule.create)
  app.post('/rule/sql', Rule.sql)
  app.get('/rule/find/:id', Rule.findById)
  app.post('/rule/find', Rule.findAll)
  app.post('/rule/update', Rule.update)
  app.post('/rule/remove', Rule.remove)
  app.post('/rule/start', Rule.start)
  app.post('/rule/stop', Rule.stop)
  app.post('/rule/isRuleHasResultApi', Rule.isRuleHasResultApi)
  app.post('/rule/result', Rule.getResult) //查找规则结果
  app.post('/rule/result/transFlow', Rule.getResultTransFlow) //查找规则结果
  app.post('/rule/result/statistic', Rule.getResultStatistic) //查找规则结果
  app.post('/rule/result/list', Rule.getResultList) //查看结果集列表
  app.post('/rule/result/list/update', Rule.getResultListUpdate) //查找规则结果
  app.post('/rule/result/uploadList', Rule.uploadList) //查找规则结果
  app.get('/rule/result/export', Rule.export) //查找规则结果

  app.post('/condition/create', Condition.create)
  app.post('/condition/remove', Condition.remove)

  app.post('/search/business', Search.businessSearch)
  app.post('/search/service', Search.serviceSearch)

  app.get('/condition/find', Condition.find)
  app.post('/condition/find', Condition.find)
  app.get('/condition/:type', Condition.find)
  app.post('/condition/update', Condition.update)
  app.post('/log/find', Log.find)
  app.post('/roleAndAuth/role', RoleAndAuth.findRole)
  app.post('/roleAndAuth/role/create', RoleAndAuth.createRole)
  app.post('/roleAndAuth/role/delete', RoleAndAuth.deleteRole)
  app.post('/roleAndAuth/role/update', RoleAndAuth.updateRole)
  app.post('/roleAndAuth/auth', RoleAndAuth.findAuth)

  app.post('/underdata', Search.underDataSearch)

  app.get('/log/test', (req, res) => {
    const errorLog = Logger.getLogger('errorLog')
    const infoLog = Logger.getLogger('infoLog')
    errorLog.error("Some debug messages1")
    errorLog.error("【===rule engine stop:】", JSON.stringify({id: '222'}))
    // infoLog.info("【===rule engine stop:】", JSON.stringify({id: '222'}))
    infoLog.info("【===rule engine stop:】")

  })

}