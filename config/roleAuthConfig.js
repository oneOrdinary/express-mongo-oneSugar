exports.auth = [
  {
    authName: '全权限',
    authEname: 'admin',
    authDesc: '拥有所有权限'
  },
  {
    authName: '监控权限',
    authEname: 'watch',
    authDesc: '监控页面的查看权限'
  },
  {
    authName: '用户管理权限',
    authEname: 'userConfig',
    authDesc: '对系统用户进行操作的权限'
  },
  {
    authName: '日志管理权限',
    authEname: 'logConfig',
    authDesc: '日志查看'
  },
  {
    authName: '规则配置权限',
    authEname: 'ruleConfig',
    authDesc: '规则配置页面相关权限'
  },
  {
    authName: '数据导出权限',
    authEname: 'dbExport',
    authDesc: '将规则运行的结果以报表形式导出的权限'
  },
  {
    authName: '数据同步权限',
    authEname: 'dbSync',
    authDesc: '将规则运行的结果通过接口同步到黄名单'
  },
  {
    authName: '查看规则配置',
    authEname: 'ruleConfigView',
    authDesc: '只能查看规则配置的相关内容，但无法修改'
  }
]

exports.adminRole = {
  cname: '管理员',
  ename: 'admin',
  desc: '全权限的管理员',
}

exports.roles = [
  {
    cname: '管理员',
    ename: 'admin',
    desc: '全权限的管理员',
    auth: ['admin']
  },
  {
    cname: '默认角色1',
    ename: 'detect',
    desc: '系统默认角色-负责条件、规则、场景的配置及相关操作',
    auth: ['ruleConfig']
  },
  {
    cname: '默认角色2',
    ename: 'dbExport',
    desc: '系统默认角色-负责规则结果里数据导出报表工作，可以查看规则配置的内容，但无法修改',
    auth: ['dbExport', 'ruleConfigView']
  },
]

