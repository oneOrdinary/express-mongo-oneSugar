/**
 * Created by hugo on 2018/3/19.
 */
const log4js = require('../../public/untils/Logger')
const reqAndRespLogger = log4js.getLogger('req')

exports.returnErrWithCode = (res, code, msg) => {
  let resp = {
    status: code,
    statusText: msg,
    data: null
  }

  reqAndRespLogger.info("resp:" + resp)

  return res.jsonp({
    status: code,
    statusText: msg,
    data: null
  })
}

exports.returnErr = (res, msg) => {
  return this.returnErrWithCode(res, 399, msg)
}

exports.returnSuccess = (res, code, msg, data) => {

  let resp = {
    status: code,
    statusText: msg,
    data: data
  }
  reqAndRespLogger.info("resp:" + resp)
  return res.jsonp({
    status: code,
    statusText: msg,
    data: data
  })
}

exports.returnSuccessExport = (res, code, msg, result) => {
  res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  res.setHeader("Content-Disposition", "attachment; filename=" + "report.xlsx");
  res.end(result, 'binary');
}




