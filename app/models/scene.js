/**
 * Created by hugo on 2018/3/14.
 */
var mongoose = require('mongoose')
var SceneSchema = require('../schemas/scene')
var Scene = mongoose.model('scenes', SceneSchema)

module.exports = Scene