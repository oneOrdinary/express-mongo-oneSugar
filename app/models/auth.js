/**
 * Created by hugo on 2018/3/14.
 */
var mongoose = require('mongoose')
var AuthSchema = require('../schemas/auth')
var Auth = mongoose.model('auth', AuthSchema)
module.exports = Auth
