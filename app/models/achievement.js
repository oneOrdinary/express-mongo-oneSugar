/**
 * Created by hugo on 2018/3/14.
 */
var mongoose = require('mongoose')
var AchievementSchema = require('../schemas/achievement')
var Achievement = mongoose.model('achievements', AchievementSchema)
module.exports = Achievement
