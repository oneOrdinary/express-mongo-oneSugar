/**
 * Created by hugo on 2018/3/14.
 */
var mongoose = require('mongoose')
var LogSchema = require('../schemas/log')
var Log = mongoose.model('log', LogSchema)

module.exports = Log
