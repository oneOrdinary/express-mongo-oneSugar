/**
 * Created by hugo on 2018/3/14.
 */
var mongoose = require('mongoose')
var RuleSchema = require('../schemas/rule')
var Rule = mongoose.model('rules', RuleSchema)

module.exports = Rule