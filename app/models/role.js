/**
 * Created by hugo on 2018/3/14.
 */
var mongoose = require('mongoose')
var RoleSchema = require('../schemas/role')
var Role = mongoose.model('role', RoleSchema)

module.exports = Role
