/**
 * Created by hugo on 2018/3/14.
 */
var Scene = require('../models/scene')
var Info = require('../../public/untils/handleBaseInfo')
var moment = require('moment')
const log4js=require('../../public/untils/Logger')
const errLogger = log4js.getLogger('err')
const sysLogger = log4js.getLogger('sys')

exports.create = (req, res) => {
  var _scene = req.body.scene

  var name = _scene.name;

  Scene.findOne({name: name}, (err, scene) => {
      if (err) {
        return Info.returnErr(res, "error:" + err)
      }
      if (scene) {
        errLogger.error("场景已存在-307")
        return Info.returnErrWithCode(res, 307, "307-场景已存在", null)
      }


      var scene = new Scene(_scene)
      //时间处理
      scene.meta.createAt = moment().format("YYYY-MM-DD HH:mm:ss")
      scene.meta.updateAt = moment().format("YYYY-MM-DD HH:mm:ss")

      if (scene._id === "") {
        scene._id = null
      }
      scene.save((err, scene) => {
        if (err) {
          errLogger.error("!!err:" + err)
          return Info.returnErr(res, err)
        }
        return Info.returnSuccess(res, 203, 'success', scene)
      })
    }
  )
}

exports.delete = (req, res) => {
  var _id = req.body._id

  Scene.remove({_id: _id}, (err) => {
      if (err) {
        return Info.returnErr(res, "error:" + err)
      }

      //返回所有的场景
      Scene.fetch((err, scenes) => {
          if (err) {
            return Info.returnErr(res, "error:" + err)
          }
          return Info.returnSuccess(res, 204, 'success', scenes)
        }
      )

    }
  )
}

exports.find = (req, res) => {
  sysLogger.info("find scenes")
  Scene.fetch((err, scenes) => {
      if (err) {
        return Info.returnErr(res, "error:" + err)
      }

      return Info.returnSuccess(res, 205, 'success', scenes)

    }
  )
}


exports.update = (req, res) => {
  var _scene = req.body.scene
  sysLogger.info("_scene update",_scene)

  _scene.meta.updateAt = Info.getNowFormatDate()

  Scene.update({_id: _scene._id}, _scene, (err, scene) => {
    if (err) {
      return Info.returnErr(res, "error Scene update:" + err)
    }

    //返回所有的数据
    Scene.fetch((err, scenes) => {
      return Info.returnSuccess(res, 206, 'success', scenes)
    })

  })
}


