/**
 * Created by hugo on 2018/3/14.
 */
const mongoose = require('mongoose')
const User = require('../models/user')
const Role = require('../models/role')
const Info = require('../../public/untils/handleBaseInfo')
const Logger = require('../../public/untils/Logger')
const logSer = require('../../app/service/LogService')
const moment = require('moment')
const log4js = require('../../public/untils/Logger')
const sysLogger = log4js.getLogger('sys')
const errLogger = log4js.getLogger('err')


exports.signup = (req, res) => {
  var _user = req.body.user

  if (!_user) {

  }
  const name = _user.name;
  const real_name = _user.real_name;
  const email = _user.email;
  User.findOne({
      $or: [
        {name: name},
        {real_name: real_name},
        {email: email}
      ]
    }, (err, user) => {
      if (err) {
        return Info.returnErr(res, "error:" + err)
      }
      if (user) {

        if (user.real_name === real_name) {
          errLogger.error("用户真实姓名-303")
          return Info.returnErrWithCode(res, 303, "303-用户名真实姓名已存在", null)
        }

        if (user.email === email) {
          errLogger.error("用户邮箱已存在-303")
          return Info.returnErrWithCode(res, 303, "303-用户邮箱已存在", null)
        }

        if (user.name === name) {
          errLogger.error("用户名已存在-303")
          return Info.returnErrWithCode(res, 303, "303-用户名已存在", null)
        }

      }

      Role.findOne({ename: _user.role}, (err, role) => {
        if (err || !role)
          return Info.returnErr(res, "error:[" + err + "]或角色不存在，请刷新后再试")

        sysLogger.info("role:", role)
        let user = new User(_user)
        user.access = "ing"
        user.meta.createAt = Info.getNowFormatDate()
        user.meta.updateAt = Info.getNowFormatDate()
        user.role = role


        user.save(function (err, user) {

          if (err) {
            return Info.returnErr(res, "error:" + err)
          }
          sysLogger.info('用户注册成功-202')
          return Info.returnSuccess(res, 202, 'success', user)
        })


      })


    }
  )
}


exports.logout = (req, res) => {
  if (!req.body.token)
    return Info.returnErr(res, "系统错误")

  sysLogger.info("用户登出")
  //清除Token
  global.user = global.user.filter(item => item !== req.body.token)
}

exports.changePwd = (req, res) => {
  var _user = req.body.user

  if (!_user) {

  }
  const name = _user.name;
  const curPassword = _user.curPassword;
  const password = _user.password;

  User.findOne({
      name: name
    }, function (err, user) {
      if (err) {
        return Info.returnErr(res, "error:" + err)
      }
      if (!user) {
        return Info.returnErr(res, "error:该用户不存在")
      }


      user.comparePassword(curPassword, function (err, isMatch) {
        if (err) {
          errLogger.error("!!err:" + err)
          return Info.returnErr(res, err)
        }
        if (isMatch) {
          //修改用户密码

          user.password = password
          user.meta.updateAt = Info.getNowFormatDate()

          user.save(function (err, user) {

            if (err) {
              return Info.returnErr(res, "error:" + err)
            }
            sysLogger.info('修改密码成功！')
            return Info.returnSuccess(res, 200, 'success', user)
          })


        } else {
          errLogger.error("用户名或密码错误-302")
          return Info.returnErrWithCode(res, 302, "302-用户名或密码错误", null)
        }
      })


    }
  )
}

exports.signin = (req, res) => {


  var _user = req.body.user
  var name = _user.name
  var password = _user.password


  User.findOne({name: name})
    .populate('role')
    .exec((err, user) => {
      if (err) {
        errLogger.error("!!err:" + err)
        return Info.returnErr(res, err)
      }

      if (!user) { //如果用户不存
        errLogger.error("用户【" + name + "】不存在-301")
        return Info.returnErrWithCode(res, 301, "301-用户不存在", null)
      }

      if (user.access === "false") {
        errLogger.error("用户【" + name + "】审核未通过，请联系管理员")
        return Info.returnErrWithCode(res, 305, "305-用户" + name + "审核未通过，请联系管理员", null)
      }

      if (user.access === "ing") {
        errLogger.error("用户【" + name + "】审核中，请稍后再试-306")
        return Info.returnErrWithCode(res, 306, "306-用户审核中，请稍后再试", null)
      }


      user.comparePassword(password, function (err, isMatch) {
        if (err) {
          errLogger.error("!!err:" + err)
          return Info.returnErr(res, err)
        }

        let curUserSize = global.user.length

        let oldUsers = global.user.filter(item => item.substring(0, 24) !== user._id.toString())

        if (oldUsers.length !== curUserSize) {
          //某用户已登录
          let token = user._id + (new Date()).valueOf()
          oldUsers.push(token)
          global.user=oldUsers
          user.token = token
        }

        user.meta.updateAt = moment().format('YYYY-MM-DD HH:mm:ss')

        if (isMatch) {
          //保存用户的状态
          sysLogger.info("用户【" + name + "】登录成功-201")

          let token = user._id + (new Date()).valueOf()


          global.user.push(token)
          user.token = token
          return Info.returnSuccess(res, 201, '用户登录成功', user)
        } else {
          errLogger.error("用户名或密码错误-302")
          return Info.returnErrWithCode(res, 302, "302-用户名或密码错误", null)
        }
      })


    })


}

exports.access = (req, res) => {
  let _id = req.body._id
  User.update({_id: _id}, {$set: {access: 'true'}})
    .then(result => {
      if (result.ok === 1)
        return Info.returnSuccess(res, 200, '更新成功', '')
    })
}

exports.changeRole = (req, res) => {
  let _id = req.body._id
  let role = req.body.role
  let changedUserNmae = req.body.changedUserName
  let operatorName = req.body.operatorName
  sysLogger.info("change role :", req.body)
  User.update({_id: _id}, {$set: {role: role}}).then(result => {
    if (result.ok === 1) {
      //日志输入
      logSer.changeUserRole(changedUserNmae, operatorName, role)
      sysLogger.info('用户权限修改成功')
      return Info.returnSuccess(res, 200, '更新成功', '')
    }
  })
}
exports.test = (req, res) => {
  var name = req.params.name
  res.send(name);
}

exports.find = (req, res) => {
  sysLogger.info("find all users")
  var _user = req.body.user
  if (!_user) {
    //查找所有的用户

    const query = req.body.query
    let queryObj = {}

    if (query.email)
      queryObj.email = query.email

    if (query.name)
      queryObj.name = query.name

    if (query.real_name)
      queryObj.real_name = query.real_name

    User.find(queryObj)
      .populate('role')
      .sort('meta.updateAt')
      .then((users) => {
        return Info.returnSuccess(res, 200, '查询成功', users)
      })
  }
}

exports.remove = (req, res) => {
  const _id = req.body._id
  let changedUserNmae = req.body.changedUserName
  let operatorName = req.body.operatorName

  sysLogger.info("remove  user id:", _id)
  User.remove({_id: _id}, (err) => {
      if (err) {
        return Info.returnErr(res, "error:" + err)
      }

      logSer.deleteUser(changedUserNmae, operatorName)

      return Info.returnSuccess(res, 200, 'success', '')
    }
  )
}


