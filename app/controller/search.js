/**
 * Created by hugo on 2018/3/14.
 */
const Info = require('../../public/untils/handleBaseInfo')
const SqlEngine = require('../service/SqlEngine')
const moment = require('moment')


exports.underDataSearch = (req, res) => {
  const hn_settle_dt = req.body.hn_settle_dt

  SqlEngine.searchUnderData(hn_settle_dt)
    .then(group => {
      let result=group[0]
      if(!result){
        return Info.returnErr(res, "数据未查询到，请更换查询日期。" )
      }
      let data=[]
      data.push({
        table_name: '数据抽取表',
        stat_result: result.stat_result.substring(0, 1),
        hn_settle_dt: moment(result.hn_settle_dt).format("YYYY-MM-DD")
      })

      data.push({
        table_name: '集群结果表',
        stat_result: result.stat_result.substring(1, 2),
        hn_settle_dt: moment(result.hn_settle_dt).format("YYYY-MM-DD")
      })

      data.push({
        table_name: '片段结果表',
        stat_result: result.stat_result.substring(2, 3),
        hn_settle_dt: moment(result.hn_settle_dt).format("YYYY-MM-DD")
      })

      data.push({
        table_name: '统计信息表（手机）',
        stat_result: result.stat_result.substring(3, 4),
        hn_settle_dt: moment(result.hn_settle_dt).format("YYYY-MM-DD")
      })


      data.push({
        table_name: '统计信息表（卡）',
        stat_result: result.stat_result.substring(4, 5),
        hn_settle_dt: moment(result.hn_settle_dt).format("YYYY-MM-DD")
      })

      data.push({
        table_name: '结果生成表',
        stat_result: result.stat_result.substring(5, 6),
        hn_settle_dt: moment(result.hn_settle_dt).format("YYYY-MM-DD")
      })

      return Info.returnSuccess(res, 200, 'success', data)
    }, err => {
      return Info.returnErr(res, "error:" + err)
    })

}


exports.businessSearch = (req, res) => {
  const queryObj = req.body.queryObj
  const pagination=req.body.pagination
  if (queryObj.type === '0') {
    //流水查询

    let result = SqlEngine.search(queryObj, 'transFlow',pagination)
    result.then(result => {
      return Info.returnSuccess(res, 200, 'success', unique(result))
    }, err => {
      return Info.returnErr(res, "error:" + err)
    })

  } else {
    //统计量查询
    SqlEngine.search(queryObj, 'statistic')
      .then(result => {
        return Info.returnSuccess(res, 200, 'success', result)
      }, err => {
        return Info.returnErr(res, "error:" + err)
      })
  }
}

exports.serviceSearch = (req, res) => {
  const queryObj = req.body.queryObj
  SqlEngine.search(queryObj, 'service')
    .then(result => {
      return Info.returnSuccess(res, 200, 'success', result)
    }, err => {
      return Info.returnErr(res, "error:" + err)
    })

}

function unique(arr) {
  var result = [], hash = {};
  for (var i = 0, elem; (elem = arr[i]) != null; i++) {
    if (!hash[elem.trans_idx]) {
      result.push(elem);
      hash[elem.trans_idx] = true;
    }
  }
  return result;
}


