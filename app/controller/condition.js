/**
 * Created by hugo on 2018/3/14.
 */
var mongoose = require('mongoose')
var Condition = require('../models/condition')
var Info = require('../../public/untils/handleBaseInfo')

const log4js = require('../../public/untils/Logger')
const sysLogger = log4js.getLogger('sys')
const errLogger = log4js.getLogger('err')

exports.create = (req, res) => {
  console.log("condition create")
  var _condition = req.body.condition
  if (_condition) {

  }

  var name = _condition.name;


  Condition.findOne({name: name}, (err, condition) => {
      if (err) {
        errLogger.error("condition.js:系统错误:"+err)
        return Info.returnErr(res, "错误:系统查询失败，请稍后再试")
      }
      if (condition) {
        errLogger.error("condition.js:条件已存在:"+err)
        return Info.returnErrWithCode(res, 300, "条件已存在", null)
      }

      condition = new Condition(_condition)
      //时间处理
      condition.meta.createAt = Info.getNowFormatDate();
      condition.meta.updateAt = Info.getNowFormatDate();

      if (condition._id === "") {
        condition._id = null
      }

      Condition.findNewId((err, record) => {
        if (err) {
          errLogger.error("condition.js-43行-findNewId:系统错误:"+err)
          return Info.returnErr(res, "系统查询错误，请稍后再试")
        }

        if (!record) {
          condition.id = '1'
        } else {
          var newId = parseInt(record.id) + 1
          condition.id = newId
        }

        condition.save((err, data) => {
          if (err) {
            errLogger.error("condition.js-56行-findNewId:保存错误:"+err)
            return Info.returnErr(res, err)
          }

          Condition.fetch((err, conditions) => {
            return Info.returnSuccess(res, 200, 'success', conditions)

          })
        })

      })
    }
  )
}

exports.remove = (req, res) => {
  var _id = req.body._id
  console.log("condition delete")
  Condition.remove({_id: _id}, (err) => {
      if (err) {
        return Info.returnErr(res, "error:" + err)
      }

      //返回所有的场景
      Condition.fetch((err, conditions) => {
          if (err) {
            return Info.returnErr(res, "error:" + err)
          }

          return Info.returnSuccess(res, 200, 'success', conditions)
        }
      )

    }
  )
}

exports.find = (req, res) => {

  const id = req.body.id
  const type = req.params.type
  const name = req.body.name

  const option = {}

  if (id)
    option.id = id

  if (type && type.trim())
    option.type = type.trim()

  if (name && name.trim())
    option.name = {$regex: name.trim()}


  Condition.find(option, (err, conditions) => {
      if (err) {
        return Info.returnErr(res, "error:" + err)
      }
      return Info.returnSuccess(res, 200, 'success', conditions)
    }
  )

}


exports.update = (req, res) => {

  console.log("condition update")
  var _condition = req.body.condition

  _condition.meta.updateAt = Info.getNowFormatDate()

  Condition.update({_id: _condition._id}, _condition, (err, condition) => {
    if (err) {
      return Info.returnErr(res, "error Scene update:" + err)
    }

    //返回所有的数据
    Condition.fetch((err, conditions) => {
      return Info.returnSuccess(res, 200, 'success', conditions)
    })

  })
}



