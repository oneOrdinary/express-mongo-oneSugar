/**
 * Created by hugo on 2018/3/14.
 */

var Rule = require('../models/rule')
var Scene = require('../models/scene')
var Info = require('../../public/untils/handleBaseInfo')
var RuleEn = require('../../app/service/RuleEngine')
var logSer = require('../../app/service/LogService')
var moment = require('moment')
const log4js = require('../../public/untils/Logger')
const errLogger = log4js.getLogger('err')
const sysLogger = log4js.getLogger('sys')

exports.create = (req, res) => {
  let _rule = req.body.rule
  Info.handleRuleScope(_rule)

  let rule = new Rule(_rule)
  Rule.findNewId((err, record) => {
    if (err) {
      errLogger.error("!!err:" + err)
      return Info.returnErr(res, err)
    }

    if (!record) {
      rule.id = 1
    } else {
      rule.id = parseInt(record.id) + 1
    }

    sysLogger.info("current time:", moment().format("YYYY-MM-DD HH:mm:ss"))
    rule.meta.updateAt = moment().format("YYYY-MM-DD HH:mm:ss")
    rule.meta.createAt = moment().format("YYYY-MM-DD HH:mm:ss")


    rule.save((err, result) => {
      if (err) {
        errLogger.error("!!err:" + err)
        return Info.returnErr(res, err)
      }
      sysLogger.info("create rule:", rule)

      logSer.createRule(rule)
      return Info.returnSuccess(res, 207, 'success', rule)
    })

  })


}

exports.update = (req, res) => {
  sysLogger.info(req.body.rule)

  var _rule = req.body.rule
  Info.handleRuleScope(_rule)

  _rule.meta.updateAt = Info.getNowFormatDate()
  Rule.update({id: _rule.id}, _rule, (err, rule) => {
    if (err) {
      return Info.returnErr(res, "error Scene update:" + err)
    }

    logSer.updateRule(_rule)

    return Info.returnSuccess(res, 209, 'success', '')
  })
}

exports.sql = (req, res) => {
  const id = req.body.id //规则id

  if (!id) {
    return Info.returnErr(res, "规则id不存在！")
  }
  RuleEn.getSql(id).then(result => {
    return Info.returnSuccess(res, 200, 'success', result)
  }, (err) => {
    return Info.returnErr(res, err)
  })


}

exports.findById = (req, res) => {
  var target = req.params.id
  sysLogger.info(target)
  Rule.findOne({id: target}, (err, rule) => {
    if (err) {
      errLogger.error("!!err:" + err)
      return Info.returnErr(res, err)
    }
    return Info.returnSuccess(res, 208, 'success', rule)
  })
}

exports.findAll = (req, res) => {
  const rule = req.body.rule

  if (!rule) {
    Rule.fetch((err, rules) => {

      sysLogger.info("find All rules", rule)

      if (err) {
        errLogger.error("!!err:" + err)
        return Info.returnErr(res, err)
      }
      return Info.returnSuccess(res, 208, 'success', {
          "list": rules
        }
      )
    })
    return
  }

  //rule存在

  if (rule.sceneId) {
    const {sceneId} = rule
    sysLogger.info("find All rules sceneID:", rule)

    Scene.find({_id: sceneId}).then(scene => {
      const option = {}
      option._id = scene[0].rulesId

      const andQueryObj = []
      if (rule.id)
        andQueryObj.push({id: rule.id})

      if (rule.person)
        andQueryObj.push({person: rule.person.trim()})

      if (andQueryObj.length > 0)
        option.$and = andQueryObj


      Rule.find(option).then(rules => {
        return Info.returnSuccess(res, 208, 'success', {
          "list": rules
        })
      })
    })
    return
  }

  let query = {}

  if (rule.id)
    query.id = parseInt(rule.id)
  if (rule.person)
    query.person = {$regex: rule.person}

  Rule.find({
      $or: [
        query,
      ]
    }, (err, rules) => {
      if (err) {
        errLogger.error("err:", err)
      }
      return Info.returnSuccess(res, 208, 'success', {
          "list": rules
        }
      )
    }
  )
}

exports.remove = (req, res) => {
  let _id = req.body._id
  let record = req.body.record
  let user = req.body.user

  if (!!record)//删除单条记录
    logSer.deleteRule(user, record)
  else { //批量删除
    let id = req.body.id
    logSer.deleteRuleGroup(user, id)
  }

  sysLogger.info('remove:', _id)

  Rule.remove({_id: _id}, (err) => {
      if (err) {
        return Info.returnErr(res, "error:" + err)
      }

      return Info.returnSuccess(res, 200, 'success', '')
    }
  )
}


exports.export = (req, res) => {
  const result_id = req.query.result_id
  const rule_id = req.query.rule_id
  const auditor = req.query.auditor


  Rule.findOne({id: rule_id}, (err, rule) => {
    if (err) {
      return Info.returnErr(res, err)
    }


    const result = RuleEn.export(result_id, rule && rule.desc, auditor)
    result.then((result) => {
      return Info.returnSuccessExport(res, 200, 'success', result)
    }, (err) => {
      return Info.returnErr(res, "error:" + err)
    })
  })

}

exports.start = (req, res) => {
  const user = req.body.user
  const rule = req.body.rule
  Rule.findOne({id: rule.id}, (err, _rule) => {
    if (err) {
      errLogger.error("!!err:" + err)
      return Info.returnErr(res, err)
    }

    if (_rule.enable === '1') { //规则已启动，返回错误
      return Info.returnErrWithCode(res, 305, "error:规则已经启动")
    }

    //向规则引擎发送
    const result = RuleEn.start(_rule)

    result.then(() => {
      logSer.startRule(user, _rule)
      //修改规则的状态
      _rule.enable = '1'
      _rule.save()

      return Info.returnSuccess(res, 200, 'success', '')
    }, (err) => {
      return Info.returnErr(res, "error:" + err)
    })
  })
}
exports.stop = (req, res) => {
  const user = req.body.user
  const rule = req.body.rule
  Rule.findOne({id: rule.id}, (err, _rule) => {
    if (err) {
      errLogger.error("!!err:" + err)
      return Info.returnErr(res, err)
    }
    if (rule.enable === '0') { //规则已关闭，返回错误
      return Info.returnErrWithCode(res, "error:规则已经关闭")
    }

    //向规则引擎发送
    const result = RuleEn.stop(_rule)

    result.then(() => {
      logSer.stopRule(user, _rule)
      //修改规则的状态
      _rule.enable = '0'
      _rule.save()

      return Info.returnSuccess(res, 200, 'success', '')
    }, (err) => {
      return Info.returnErr(res, "error:" + err)
    })
  })
}

exports.isRuleHasResultApi = (req, res) => {
  const rule_id = req.body.rule_id

  const result = RuleEn.hasResult(rule_id)

  result.then(result => {
    if (result.length !== 0)
      return Info.returnSuccess(res, 200, 'success', '')
    else
      return Info.returnErr(res, "error:系统错误")
  })

}

exports.uploadList = (req, res) => {
  const result_id = req.body.result_id
  const rule_id = req.body.rule_id

  Rule.findOne({id: rule_id}, (err, rule) => {
    if (err) {
      errLogger.error("!!err:" + err)
      return Info.returnErr(res, err)
    }

    let result = RuleEn.uploadList(result_id, rule)
    result.then(result => {
      return Info.returnSuccess(res, 200, 'success', result)
    }, err => {
      return Info.returnErr(res, "error:" + err)
    })

  })


}


exports.getResult = (req, res) => {
  const queryResult = req.body.result
  const pagination = req.body.pagination
  sysLogger.info("-----getResult:", queryResult)
  let result = RuleEn.getResult(queryResult, pagination)
  result.then(result => {
    return Info.returnSuccess(res, 200, 'success', result)
  }, err => {
    return Info.returnErr(res, "error:" + err)
  })
}

exports.getResultStatistic = (req, res) => {
  const queryResult = req.body.result
  sysLogger.info("-----getResultStatistic:", queryResult)
  let result = RuleEn.getResultStatistic(queryResult)
  result.then(result => {
    return Info.returnSuccess(res, 200, 'success', result)
  }, err => {
    return Info.returnErr(res, "error:" + err)
  })
}

exports.getResultTransFlow = (req, res) => {
  const queryResult = req.body.result
  sysLogger.info("-----getResultTransFlow:", queryResult)
  let result = RuleEn.getResultTransFlow(queryResult)
  result.then(result => {
    return Info.returnSuccess(res, 200, 'success', result)
  }, err => {
    return Info.returnErr(res, "error:" + err)
  })
}


exports.getResultListUpdate = (req, res) => {
  const updateResultList = req.body.updateResultList
  const id = req.body.id
  const username = req.body.username
  sysLogger.info("-----getResultListUpdate:", updateResultList)
  sysLogger.info("-----id:", id)

  let result = RuleEn.getResultListUpdate(updateResultList, id, username)

  result.then(() => {
    return Info.returnSuccess(res, 200, 'success', '')
  }, err => {
    return Info.returnErr(res, "error:" + err)
  })
}


exports.getResultList = (req, res) => {
  const queryResult = req.body.resultList
  sysLogger.info("-----getResultList:", queryResult)

  let result = RuleEn.getResultList(queryResult)

  result.then(result => {
    return Info.returnSuccess(res, 200, 'success', result)
  }, err => {
    return Info.returnErr(res, "error:" + err)
  })
}
