/**
 * Created by hugo on 2018/3/14.
 */
var Role = require('../models/role')
var Auth = require('../models/auth')
var Info = require('../../public/untils/handleBaseInfo')
const log4js=require('../../public/untils/Logger')
const errLogger = log4js.getLogger('err')

exports.createRole = (req, res) => {
  let _role = req.body.role
  const ename = _role.ename
  Role.findOne({ename: ename}, (err, role) => {
    if (err) {
      return Info.returnErr(res, "error:" + err)
    }
    if (role) {
      errLogger.error("createRole Error : 角色已存在 ！")
      return Info.returnErrWithCode(res, 300, "角色已存在", null)
    }

    role = new Role(_role)

    role.save((err, role) => {
      if (err) {
        errLogger.error("!!err:" + err)
        return Info.returnErr(res, err)
      }
      return Info.returnSuccess(res, 200, '创建角色成功', role)
    })
  })
}

exports.updateRole = (req, res) => {
  let _role = req.body.role
  Role.update({ename: _role.ename}, _role, (err, role) => {
    if (err) {
      return Info.returnErr(res, "error:" + err)
    }

    return Info.returnSuccess(res, 200, '更新角色成功', '')

  })
}

exports.deleteRole = (req, res) => {
  const ename = req.body.ename
  Role.remove({ename: ename}, (err, role) => {
    if (err) {
      return Info.returnErr(res, "删除角色失败 error:" + err)
    }

    return Info.returnSuccess(res, 200, '删除角色成功', '')

  })
}

exports.findRole = (req, res) => {
  const role = req.body.role

  const query = {}

  if (role && !!role.ename.trim())
    query.ename = role.ename.trim()


  if (role && !!role.cname.trim())
    query.cname = role.cname.trim()


  Role.find(query).sort({'date': -1})
    .then((roles) => {
      return Info.returnSuccess(res, 200, '查询成功', roles)
    })
}

exports.findAuth = (req, res) => {

  Auth.find({}).sort({'date': -1})
    .then((auth) => {
      return Info.returnSuccess(res, 200, '查询成功', auth)
    })
}


