/**
 * Created by hugo on 2018/3/14.
 */
var Log = require('../models/log')
var Info = require('../../public/untils/handleBaseInfo')

exports.find = (req, res) => {
  const log = req.body.log

  const query = {}
  if (log && log.user)
    query.user = {$regex: log.user.trim()}

  if (log && log.date)
    query.date = {$regex: log.date.split("T")[0]}


  Log.find(query).sort({'date': -1})
    .then((logs) => {
      return Info.returnSuccess(res, 200, '查询成功', logs)
    })
}


