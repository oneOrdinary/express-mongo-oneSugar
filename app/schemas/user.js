/**
 * Created by hugo on 2018/3/13.
 */
var mongoose = require('mongoose')
const crypto = require("crypto");
var Schema = mongoose.Schema
var ObjectId = Schema.Types.ObjectId
var Number = Schema.Types.Number

var UserSchema = new mongoose.Schema({
  id: {
    unique: true, //唯一
    type: String  //类型
  },
  email: {
    unique: true, //唯一
    type: String  //类型
  },
  password: String,
  // nomal user
  // admin
  access: String,//账号是否审核通过 true-审核通过 false-审核拒绝 ing-审核中
  name: String,//昵称
  real_name: String,
  //admin| service | db | detect |
  role: {
    ref: 'role',
    type: ObjectId
  },
  token: String, //临时令牌
  userSys: {//用户体系
    level: String,//等级
  },
  meta: {
    createAt: {
      type: String,
    },
    updateAt: {
      type: String,
    }
  }
})

UserSchema.pre('save', function (next) {
  let user = this
  let md5 = crypto.createHash("md5");
  let password = user.password;
  let newPas = md5.update(password).digest("hex");
  user.password = newPas
  next()
})


UserSchema.statics = {
  fetch: function (cb) {
    return this
      .find({})
      .sort('meta.updateAt')
      .exec(cb)
  },
  findById: function (id, cb) {
    return this
      .findOne({_id: id})
      .exec(cb)
  },
}

//实例才可调用的方法
UserSchema.methods = {
  comparePassword: function (_password, cb) {
    let md5 = crypto.createHash("md5");
    let newPas = md5.update(_password).digest("hex");
    if (newPas === this.password) {
      cb(null, true)
    } else {
      cb(false)
    }
  },
}


// midware for user
exports.signinRequired = function (req, res, next) {
  var user = req.session.user

  if (!user) {
    return res.redirect('/signin')
  }

  next()
}

module.exports = UserSchema