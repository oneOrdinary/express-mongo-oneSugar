/**
 * Created by hugo on 2018/3/13.
 */
var mongoose = require('mongoose')

var LogSchema = new mongoose.Schema({
  date: String , // 日志时间
  //日志类型:创建create、修改update、删除delete、启动run、停止stop
  operType:String,
  object:String, //操作对象，规则-rule、场景-scene、条件-condition、用户-user、
  user:String,//行为人
  content:String,//日志内容
})

// UserSchema.pre('save', function(next) {
//
// })


LogSchema.statics = {
  fetch: function(cb) {
    return this
      .find({})
      .sort('meta.updateAt')
      .exec(cb)
  },
}



module.exports = LogSchema