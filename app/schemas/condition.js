/**
 * Created by hugo on 2018/3/13.
 */
var mongoose = require('mongoose')
var moment = require('moment')

var ConditionSchema = new mongoose.Schema({
  id: {
    unique: true, //唯一
    type: Number  //类型
  }, //标识
  name: String, //条件的名称
  type: String,//类型，自身条件、外界条件

  logic: {
    type: String,
    default: 'and'
  },//条件与条件之间的逻辑关系，只有在配置规则时存在
  editable: {
    type: Boolean,
    default: false
  },//是否可编辑，只在配置规则时存在


  attr: [{
    isCal: Boolean, //是否是计算式
    label: String,
    fields: {
      field1: String,
      field2: String,
      symbol: String
    }, //字段名
    valueType: String,
    relationship: String,
    threshold: String,
    logic: String,
    unit: String,
  }],
  outAttr: [{
    objectName: String, //对象
    label: String,
    logic: String,//逻辑关系，不是and就是or
    attr: [{
      field: String,//字段
      label: String,
      valueType: String,//值类型
      relationship: String,//关系
      threshold: String,//阈值
      logic: String,
      unit: String,//单位
    }]
  }],
  meta: {
    createAt: {
      type: String,
      default:moment().format("YYYY-MM-DD HH:mm:ss")
    },
    updateAt: {
      type: String,
      default:moment().format("YYYY-MM-DD HH:mm:ss")
    }
  }
})

ConditionSchema.statics = {
  fetch: function (cb) {
    return this
      .find({})
      .sort('id')
      .exec(cb)
  },
  findById: function (id, cb) {
    return this
      .findOne({_id: id})
      .exec(cb)
  },
  findNewId: function (cb) {
    return this
      .findOne()
      .sort({'id': -1})
      .exec(cb)
  }
}



module.exports = ConditionSchema