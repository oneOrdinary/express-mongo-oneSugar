/**
 * Created by hugo on 2018/3/13.
 */

var mongoose = require('mongoose')
var SceneSchema = new mongoose.Schema({
  name: {
    unique: true, //唯一
    type: String  //类型
  },
  desc: String,  //描述
  rulesId:[],//包含rules的id
  meta: {
    createAt: {
      type: String,
    },
    updateAt: {
      type: String,
    }
  }
})

SceneSchema.statics = {
  fetch: function (cb) {
    return this
      .find({})
      .sort({'meta.createAt':-1})
      .exec(cb)
  },

  findByName: function (name, cb) {
    return this
      .findOne({name: name})
      .exec(cb)
  },
}
module.exports = SceneSchema