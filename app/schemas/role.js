/**
 * Created by hugo on 2018/3/13.
 */

var mongoose = require('mongoose')
var RoleSchema = new mongoose.Schema({
  cname: String , // 角色名称
  ename: String , // 角色名称
  desc:String, //角色描述
  auth:[{
    type: String
  }],//权限
})



RoleSchema.statics = {
  fetch: function(cb) {
    return this
      .find({})
      .sort('meta.updateAt')
      .exec(cb)
  },
}



module.exports = RoleSchema