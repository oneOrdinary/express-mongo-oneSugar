/**
 * Created by hugo on 2018/3/13.
 */
var mongoose = require('mongoose')

var AuthSchema = new mongoose.Schema({
  authName: String, // 权限名称
  authEname: String, // 权限名称
  authDesc: String, //权限描述描述
})


AuthSchema.statics = {
  fetch: function (cb) {
    return this
      .find({})
      .exec(cb)
  },
}


module.exports = AuthSchema