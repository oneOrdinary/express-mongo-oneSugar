/**
 * Created by hugo on 2018/3/13.
 */
var moment = require('moment')
var mongoose = require('mongoose')

var RuleSchema = new mongoose.Schema({
  id: {
    type: Number,
    unique: true, //唯一
  },
  sid: String, //场景id
  name: String, //规则名称
  desc: String, //规则描述
  object: String, //规则对象
  person: String, //配置人
  type: String, //规则类型
  enable: {
    type: String,
    default: '0'
  },//规则是否启用
  date: String, //规则日期
  timeSep: String, //规则启动的时间
  scope: { //规则作用的时间范围
    startTime: String,
    endTime: String
  },
  frequence: {
    freqType: String, //daySep,weekSep,today,delay
    freqNum: String,
  }, //频率
  scoreType: [{ //评分筛选类型
    score: {  //评分区间
      startScore: String,
      endScore: String,
    },

    detail: String
  }],
  ruleType: [{ //多个条件快
    selfAttr: [{
      key: String,
      logic: String,
      inAttr: [{//条件组
        name: String, //条件组的具体名称
        logic: String,
        attr: [{ //条件
          fields: { //算式表达
            field1: String,
            field2: String,
            symbol: String,//"Add, sub, mul and div"
          },
          isCal: { //是否是算式
            type: Boolean,
            default: false
          },
          label: String,
          valueType: String,
          relationship: String,
          threshold: String,
          logic: String,
          unit: String,
        }]
      }],
    }],
    outAttr: [{
      objectName: String, //对象
      label: String,
      logic: String,//逻辑关系，不是and就是or
      hasOutDateOpt: Boolean,
      attr: [{
        field: String,//字段
        label: String,
        valueType: String,//值类型
        relationship: String,//关系
        threshold: String,//阈值
        logic: String,
        unit: String,//单位
      }]
    }],
    logic: String, //条件与条件之间的逻辑关系
    statistic: {  //
      field: String,//统计量
      relationship: String, //关系
      threshold: String, //阈值
      score: String,//分数
    },

  }],
  meta: {
    createAt: {
      type: String,
      default: moment().format("YYYY-MM-DD HH:mm:ss")
    },
    updateAt: {
      type: String,
      default: moment().format("YYYY-MM-DD HH:mm:ss")
    }
  }
})

RuleSchema.statics = {
  fetch: function (cb) {
    return this
      .find({})
      .sort(``)
      .exec(cb)
  },

  fetchSearch: function (params, cb) {
    return this
      .find({params})
      .sort({'meta.createAt': -1})
      .exec(cb)
  },

  findById: function (id, cb) {
    return this
      .findOne({id: id})
      .exec(cb)
  },
  findNewId: function (cb) {
    return this
      .findOne()
      .sort({'id': -1})
      .exec(cb)
  }
}


module.exports = RuleSchema
