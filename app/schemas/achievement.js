/**
 * Created by hugo on 2018/8/17.
 */
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectId

let AchievementSchema = new mongoose.Schema({
  name: String,//名称
  desc: String,//描述
  persons: [{
    type: ObjectId,
    ref: 'users'
  }],
  lastPerson: String,//最近完成
  firstPerson: String,//完成第一人
  num: String,//成就完成总人数
})

module.exports = AchievementSchema