/**
 * Created by hugo on 2018/5/7.
 */

const envJs = require('../../config/env')
const env = process.env.NODE_ENV === 'dev' ? envJs.devConf :
  process.env.NODE_ENV === 'test' ? envJs.testConf : envJs.proudConf

const log4js = require('../../public/untils/Logger')
const errLogger = log4js.getLogger('err')
const sysLogger = log4js.getLogger('sys')

const startUrl = "http://" + env.serviceHost + ":" + env.servicePort + "/hn/runRule.action"
const stopUrl = "http://" + env.serviceHost + ":" + env.servicePort + "/hn/haltRule.action"
const uploadUrl = "http://" + env.serviceHost + ":" + env.servicePort + "/hn/sendBill.action"


const request = require('request')
const SQL = require('./SqlEngine')
const xlsx = require('node-xlsx').default;
const fs = require('fs')

exports.start = (rule) => {
  let uuid = log4js.getLogId()
  sysLogger.info("[log_id:" + uuid + "]rule engine start:", JSON.stringify(rule))
  const options = {
    url: startUrl,
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      'log_id': uuid
    },
    body: JSON.stringify(rule)
  }

  return new Promise((resolve, reject) => {
    request(options, (err, resp) => {
      if (!!err || !resp || !resp.body) {
        errLogger.error("RuleEngine【===err102:启动错误===】 err:" + err)
        reject(err ? err : '系统错误')
        return
      }

      let result
      try {
        result = JSON.parse(resp.body)
      } catch (err) {
        errLogger.error("规则启动失败:服务端返回数据为null请稍后再试！")
        reject('规则启动失败:服务端返回数据为null请稍后再试！')
      }

      if (result.responseCode !== '00') {

        errLogger.error("RuleEngine【===err:启动错误99===】 responseCode:" + result.responseCode +
          " message:" + result.responseMsg)
        reject('规则启动失败:' + result.responseMsg + ' 请稍后再试！')
      }

      resolve()

    })
  })
}
exports.stop = (rule) => {
  const options = {
    url: stopUrl,
    method: 'post',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({id: rule.id})
  }

  sysLogger.info("[log_id:" + log4js.getLogId() + "] rule engine stop id:", JSON.stringify({id: rule.id}))

  return new Promise((resolve, reject) => {
    request(options, (err, resp) => {
      if (!!err) {
        errLogger.error("RuleEngine【===err100:停止错误===】 err:" + err)
        reject(err)
      }

      const result = JSON.parse(resp ? resp.body : '')

      resolve()

      if (result.responseCode !== '00') {
        errLogger.error("RuleEngine【===err101:规则停止失败===】 responseCode:" + result.responseCode)
        reject('规则停止失败:' + result.responseMsg + ' 请稍后再试！')
      }
      resolve()

    })
  })
}

exports.getSql = (id) => {

  let modSql = 'SELECT rule_sql  FROM tbl_acscp_rule_status ' +
    'where id=' + id


  sysLogger.info("RuleEngine【===getSql ===】:", modSql)

  return SQL.query(modSql, [])
    .then(results => {
      return new Promise((resolve, reject) => {
        resolve(results.length > 0 ? results[0].rule_sql : '暂无')
      })
    })
}


exports.getResult = (query, pagination) => {


  let modSql = 'SELECT id,auditor,generate_date,hn_settle_dt,object,result_num,' +
    'run_status,time_frame,check_status,rule_id,error_info FROM tbl_acscp_rule_result ' +
    'where 1=1 ';

  if (query && !!query.rule_id)
    modSql += ' and rule_id like ' + SQL.poolEscape(query.rule_id)
  if (query && !!query.auditor)
    modSql += ' and auditor like ' + SQL.poolEscape(query.auditor.toString().trim())
  if (query && !!query.generate_date)
    modSql += ' and generate_date like  ' + SQL.poolEscape('%' + query.generate_date.split('T')[0] + '%')


  modSql += ' order by generate_date desc'

  sysLogger.info("RuleEngine【===getResult SQL ===】:", modSql)

  return SQL.query(modSql, [])
}

exports.getResultList = (query) => {

  //const connection = SQL.getConnection()

  let modSql = 'SELECT id,object,	result,rule_id,object,hn_settle_dt,' +
    '	result_id ,status FROM tbl_acscp_scalper_list ' +
    'where 1=1'


  if (query && !!query.result_id)
    modSql += ' and result_id like ' + SQL.poolEscape(query.result_id)

  if (query && !!query.result)
    modSql += ' and result like ' + SQL.poolEscape('%' + query.result + '%')

  sysLogger.info("RuleEngine【=== getResultList SQL ===】:", modSql)

  return SQL.query(modSql, [])
}

exports.getResultListUpdate = (query, id, username) => {


  let status1Id = '('
  let status2Id = '('
  query.list.map((v, i) => {
    if (v.status === 1) {
      status1Id += v.id + ','
    }
    if (v.status === 2) {
      status2Id += v.id + ','
    }

  })

  status1Id = status1Id.substring(0, status1Id.length - 1) + ')'
  status2Id = status2Id.substring(0, status2Id.length - 1) + ')'

  sysLogger.info("RuleEngine【=== status1Id ===】:", status1Id)
  sysLogger.info("RuleEngine【=== status2Id ===】:", status2Id)

  let modSqlStatus1 = 'update tbl_acscp_scalper_list set status = 1 where id in ' + status1Id
  let modSqlStatus2 = 'update tbl_acscp_scalper_list set status = 2 where id in ' + status2Id

  //把结果集状态从待待审核改成已审核
  let updateResultSql = 'update tbl_acscp_rule_result set check_status = 1, auditor = '
    + SQL.poolEscape(username) + ' where id = ' + SQL.poolEscape(id)


  return new Promise((resolve, reject) => {
    if (status1Id !== ')') {
      sysLogger.info("RuleEngine【=== modSqlStatus1 ===】:", modSqlStatus1)
      SQL.query(modSqlStatus1, []).then(result => {
        resolve()
      }, (err) => reject(err))
    } else if (status2Id !== ')')
      resolve()


  }).then(() => {
    return new Promise((resolve, reject) => {
      if (status2Id !== ')') {
        sysLogger.info("RuleEngine【=== modSqlStatus2 ===】:", modSqlStatus2)
        SQL.query(modSqlStatus2, []).then(result => {
          resolve()
        }, (err) => reject(err))
      } else
        resolve()

    })
  }).then(() => {
    return new Promise((resolve, reject) => {
      //更新当前记录的状态
      sysLogger.info("RuleEngine【=== updateResultSql ===】:", updateResultSql)
      SQL.query(updateResultSql, []).then(result => {
        resolve()
      }, (err) => reject(err))
    })
  })
}


exports.getResultTransFlow = (query) => {
  sysLogger.info("RuleEngine【=== getResultTransFlow queryObj===】:", JSON.stringify(query))

  const hnSettleDts = query.hnSettleDt.split('-')
  const hnSettleDt = hnSettleDts[1] + hnSettleDts[2]


  const queryObjSql = query.object === '0' ? ' phone_no= ' + SQL.poolEscape(query.result) :
    ' pri_acct_no= ' + SQL.poolEscape(query.result)

  let colStr1 = '*'
  let modSql = 'SELECT ' + colStr1 + ' FROM tbl_acscp_res_flow' + hnSettleDt +
    ' where result_id=' + SQL.poolEscape(query.id) + ' and ' + queryObjSql

  sysLogger.info("RuleEngine【===getResultTransFlow SQL ===】:", modSql)

  return SQL.query(modSql, [])

}

exports.getResultStatistic = (query) => {

  const hnSettleDts = query.hnSettleDt.split('-')
  const hnSettleDt = hnSettleDts[1] + hnSettleDts[2]

  let queryObj = ''
  switch (query.object) {
    case '0':
      queryObj = 'phone_no='
      break
    case '1':
      queryObj = 'pri_acct_no='
      break
    case '2':
      queryObj = 'term_id=' //终端号
      break
    case '3':
      queryObj = 'mchnt_cd=' //商户号
      break
    case '4':
      queryObj = 'device_id=' //设备号
      break
  }
  const queryObjSql = ' ' + queryObj + SQL.poolEscape(query.result)

  let modSql = 'SELECT * FROM tbl_acscp_stat_cnt' + hnSettleDt +
    ' where result_id=' + SQL.poolEscape(query.id) + ' and ' + queryObjSql

  if (query && !!query.rule_id)
    modSql += ' and rule_id like ' + SQL.poolEscape(query.rule_id)
  if (query && !!query.auditor)
    modSql += ' and auditor like ' + SQL.poolEscape('%' + query.auditor + '%')
  else if (query && !!query.generate_date)
    modSql += ' and generate_date like '
      + SQL.poolEscape('%' + query.generate_date.split('T')[0] + '%')


  sysLogger.info("RuleEngine【=== getResultStatistic SQL ===】:", modSql)

  return SQL.query(modSql, [])

}


exports.uploadList = (result_id, rule) => {

  return new Promise((resolve, reject) => {

    if (!rule) {
      errLogger.error("RuleEngine【===err109:同步错误===】:同步规则未找到")
      reject('同步规则未找到')
    }

    const options = {
      url: uploadUrl,
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        result_id: result_id,
        rule_name: rule.name,
        rule_desc: rule.desc ? rule.desc : '暂未填写说明',
        object: rule.object
      })
    }

    sysLogger.info("RuleEngine【===rule engine upload===:】", JSON.stringify(options))

    request(options, (err, resp) => {
      if (!!err) {
        errLogger.error("RuleEngine【===err105:同步错误===】 err:" + err)
        reject(err)
      }

      const result = JSON.parse(resp ? resp.body : '')

      if (result.responseCode !== '00') {
        errLogger.error("RuleEngine【===err106:黄名单同步失败===】 responseCode:" + result.responseCode)
        reject('黄名单同步失败:' + result.responseMsg + ' 请稍后再试！')
      }
      resolve()
    })
  })
}

exports.export = (result_id, desc, auditor) => {
  return this.getResultList({result_id: result_id})
    .then((result) => {

        const data2 = [
          {
            name: '黄名单',
            data: [
              [{
                v: '序号',
              },
                {
                  v: '证件号',
                },
                '手机号',
                '银行卡号',
                '终端号',
                '商户号',
                '设备号',
                '用户ID',
                '置信度',
                '名单来源', '名单类别     ', '名单生产模式', '操作标识', '名单产生原因'],
            ]
          },

        ]

        result.map((v, i) => {

          let objectStr = {
            type: v.object,
            data: v.result
          }


          objectStr.data = v.result
          data2[0].data.push([
            {
              v: v.id, //序号
            },
            {
              v: '', //证件号
            },
            {
              v: objectStr.type === '0' ? objectStr.data : '',//手机号

            },
            {
              v: objectStr.type === '1' ? objectStr.data : '',//银行卡号

            },
            {
              v: objectStr.type === '2' ? objectStr.data : '',//终端号
            },
            {
              v: objectStr.type === '3' ? objectStr.data : '',//商户号
            },
            {
              v: objectStr.type === '4' ? objectStr.data : '',//设备号
            },
            {
              v: ''
            },
            {
              v: 100, //   置信度
            },

            {
              v: '000000000005', //名单来源系统
            },
            {
              v: '03',//名单类别
            },
            {
              v: '01', //名单生产模式
            },
            {
              v: '1', //操作标识
            },
            {
              v: desc || '所属规则已删除', //名单产生原因
            }
          ])
        })

        const option = {
          '!cols': [{wpx: '80', wch: '150'}, {wpx: '80', wch: '150'}, {wpx: '120', wch: '150'}, {
            wpx: '100',
            wch: '150'
          }],

          defaultCellStyle: {
            font: {name: "Verdana", sz: 11, color: "FF00FF88"},
            fill: {fgColor: {rgb: "FFFFAA00"}}
          }

        }

        const buffer = xlsx.build(data2, option); // Returns a buffer

        return buffer

      }
    )

}


exports.hasResult = (rule_id) => {

  let modSql = 'SELECT id FROM tbl_acscp_rule_result ' +
    'where rule_id=' + SQL.poolEscape(rule_id)

  sysLogger.info("RuleEngine【=== hasResult modSql ===】:", modSql)

  return SQL.query(modSql, [])

}

