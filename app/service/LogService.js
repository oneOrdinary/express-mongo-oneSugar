/**
 * Created by hugo on 2018/5/7.
 */
var request = require('request');
var Log = require('../models/log')
var moment = require('moment')
const log4js=require('../../public/untils/Logger')
const errLogger = log4js.getLogger('err')
const sysLogger = log4js.getLogger('sys')

exports.startRule = (user, rule) => {
  const curTime = moment().format("YYYY-MM-DD HH:mm:ss")
  let log = {
    date: curTime,
    operType: 'run',
    object: 'rule' + '| GZ' + rule.id,
    user: user.real_name,
    content: '用户【' + user.real_name + '】于' + curTime + ' 【启动】规则 【GZ' + rule.id + '】 -' + rule.name
  }

  const data = new Log(log)
  data.save((err, log) => {
    if (err)
      errLogger.error(err)
  })

}

exports.stopRule = (user, rule) => {
  const curTime = moment().format("YYYY-MM-DD HH:mm:ss")
  let log = {
    date: curTime,
    operType: 'stop',
    object: 'rule' + '| GZ' + rule.id,
    user: user.real_name,
    content: '用户【' + user.real_name + '】于' + curTime + ' 【停止】规则 【GZ' + rule.id + '】 -' + rule.name
  }

  const data = new Log(log)
  data.save((err, log) => {
    if (err)
      errLogger.error(err)
  })

}
exports.deleteRule = (user, rule) => {
  sysLogger.info("delete-rule-log", rule)
  const curTime = moment().format("YYYY-MM-DD HH:mm:ss")
  let log = {
    date: curTime,
    operType: 'delete',
    object: 'rule' + '| GZ' + rule.id,
    user: user.real_name,
    content: '用户【' + user.real_name + '】于' + curTime + ' 【删除】规则 【GZ' + rule.id + '】 -' + rule.name
  }

  const data = new Log(log)
  sysLogger.info("delete-rule-log:", data)
  data.save((err, log) => {
    if (err)
      errLogger.error(err)
  })

}

exports.deleteUser = (changedName, operatorName) => {
  const curTime = moment().format("YYYY-MM-DD HH:mm:ss")
  let log = {
    date: curTime,
    operType: 'delete',
    object: '用户删除',
    user: operatorName,
    content: '用户【' + operatorName + '】于' + curTime + ' 【删除】用户【' + changedName + '】'
  }

  const data = new Log(log)
  data.save((err, log) => {
    if (err)
      errLogger.error(err)
  })

}

exports.changeUserRole = (changedName, operatorName, newRole) => {
  const curTime = moment().format("YYYY-MM-DD HH:mm:ss")

  let log = {
    date: curTime,
    operType: 'update',
    object: '权限变动',
    user: operatorName,
    content: '用户【' + operatorName + '】于' + curTime + ' 【更新】 用户 【' + changedName + '】权限为' + newRole.cname
  }

  const data = new Log(log)
  data.save((err, log) => {
    if (err)
      errLogger.error(err)
  })

}

exports.deleteRuleGroup = (user, ids) => {
  sysLogger.info("delete-rule-log", ids)
  let str = ''
  const curTime = moment().format("YYYY-MM-DD HH:mm:ss")
  ids.map((v, i) => {
    str += "| GZ-" + v
  })

  let log = {
    date: curTime,
    operType: 'delete',
    object: 'rule' + str,
    user: user.real_name,
    content: '用户【' + user.real_name + '】于' + curTime + ' 【批量删除】规则 【' + str + '】'
  }

  const data = new Log(log)
  errLogger.error("delete-rule-log:", data)
  data.save((err, log) => {
    if (err)
      errLogger.error(err)
  })

}

exports.createRule = (rule) => {
  sysLogger.info("create-rule-log", rule)
  let log = {
    date: rule.meta.createAt,
    operType: 'create',
    object: 'rule' + '| GZ' + rule.id,
    user: rule.person,
    content: '用户【' + rule.person + '】于' + rule.meta.createAt + ' 【创建】规则 【GZ' + rule.id + '】 -' + rule.name
  }

  const data = new Log(log)
  sysLogger.info("create-rule-log:", data)
  data.save((err, log) => {
    if (err)
      errLogger.error(err)
  })

}

exports.updateRule = (rule) => {
  let log = {
    date: rule.meta.updateAt,
    operType: 'update',
    object: 'rule' + '| GZ' + rule.id,
    user: rule.person,
    content: '用户【' + rule.person + '】于' + rule.meta.updateAt + ' 【更新】规则 【GZ' + rule.id + '】 -' + rule.name
  }
  const data = new Log(log)
  sysLogger.info("update-rule-log:", data)
  data.save((err, log) => {
    if (err)
      errLogger.error(err)
  })
}