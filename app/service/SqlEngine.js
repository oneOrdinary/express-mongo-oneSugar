/**
 * Created by hugo on 2018/5/7.
 */
const log4js = require('../../public/untils/Logger')
const sysLogger = log4js.getLogger('sys')
const errLogger = log4js.getLogger('err')

const moment = require('moment')
const envJs = require('../../config/env')
const env = process.env.NODE_ENV === 'dev' ? envJs.devConf :
  process.env.NODE_ENV === 'test' ? envJs.testConf : envJs.proudConf

let pool
const SQL = require('./SqlEngine')
exports.createConnPool = (sqlPassword) => {
  const mysql = require('mysql');

  sysLogger.info("【SQl 连接数据】 host:" + env.sqlHost + "| user: " + env.sqlUser + " | " +
    " port: " + env.sqlPort + " | database:" + env.sqlDatabase + "| password:" + sqlPassword)

  const poolOpt = {
    host: env.sqlHost,
    user: env.sqlUser,
    password: sqlPassword ? sqlPassword : env.sqlPassword,
    port: env.sqlPort,
    database: env.sqlDatabase
  }

  pool = mysql.createPool(poolOpt)

  pool.on('error',(err)=>{
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      errLogger.error('数据库失去连接们正在尝试切换……')

    } else {
      throw err;
    }
  })
}

exports.getPool = () => {

  return pool
}
exports.poolEscape = (param) => {

  return pool.escape(param)
}


exports.query = (sql, opt) => {
  if(!pool){

  }
  return new Promise((resolve, reject) => {
    pool.query(sql, opt, (err, results) => {
      if (err) {
        errLogger.error('[query ERROR] - ', err.message)
        reject('数据库查询失败：请稍后再试或联系管理员处理！')
        return
      }
      resolve(results)

    })

  })
}
exports.search = (query, type, pagination) => {

  let queryObject = ''
  switch (query.object) {
    case '0':
      queryObject = 'phone_no'
      break
    case '1':
      queryObject = 'pri_acct_no '
      break
    case '2':
      queryObject = 'term_id'
      break
    case '3':
      queryObject = 'mchnt_cd'
      break
    case '4':
      queryObject = 'device_id'
      break
  }


  let tableFront = ''
  if (type === 'service') {
    sysLogger.info("SqlEngine【=== search service queryObj===】:", JSON.stringify(query))
    let modSql = 'SELECT * FROM tbl_acscp_scalper_list' +
      ' where result = ' + SQL.poolEscape(query.value)+' and status = 1'

    sysLogger.info("SqlEngine【=== service search SQL ===】:", modSql)

    return SQL.query(modSql, [])

  } else if (type === 'statistic') {
    tableFront = 'tbl_acscp_stat_cnt'
  } else {
    tableFront = 'tbl_acscp_res_flow'
  }

  sysLogger.info("SqlEngine【=== search business queryObj===】:", JSON.stringify(query))

  let tmpTime = query.time_scope[0]
  const timeFlag = []
  while (true) {
    const reg = new RegExp("-", "g");
    if (moment(tmpTime).diff(query.time_scope[1], 'days') === 1)
      break

    timeFlag.push(tmpTime.replace(reg, '').substring(4))
    tmpTime = moment(tmpTime).add(1, 'days').format("YYYY-MM-DD")

  }

  let sqlStr = ''
  let sqlTmp=''
  timeFlag.map((v, i) => {
    sqlTmp = 'select * from ' + tableFront + v + ' where ' + queryObject + ' =' + SQL.poolEscape(query.value)
    if (i !== timeFlag.length - 1){
      sqlTmp += ' union all '
    }


    sqlStr += sqlTmp
  })

  sqlStr+=' limit 500' //限制800条数据
  sysLogger.info("SqlEngine【===search SQL sqlStr ===】:", sqlStr)

  return SQL.query(sqlStr, [])

}


exports.searchUnderData = (hn_settle_dt) => {

  let modSql = 'Select * from tbl_acscp_stat_result where hn_settle_dt = ' + SQL.poolEscape(hn_settle_dt)
  sysLogger.info("SqlEngine【=== searchUnderData===】:", modSql)

  return SQL.query(modSql, [])


}